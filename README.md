# README #

Heuristic Algorithms for the Adjacent Only Quadratic Minimum Spanning Tree Problem based on Variable Neighborhood Descent and Simulated Annealing.

### How do I get set up? ###

- Dependencies: Python 3.6

- How to run tests: 
python3 local_search/Main_Path.py [instance_file]
or
python3 local_search/Main_Tree.py [instance_file]