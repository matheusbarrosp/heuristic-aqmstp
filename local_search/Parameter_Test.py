import sys
import Instance
import instance_reader
import Constructive_Heuristic
import Tree
import Local_Search
import random
import time

instance_file = sys.argv[1]
instance = Instance.Instance(instance_reader.read_instance(instance_file))

global_iterations = [10, 25, 50, 75, 100, 150, 200]
vnd_iterations = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
vnd_attempts = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
perturbation = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
temperature_att = [0.95, 0.96, 0.97, 0,98, 0.99]

def evaluate(solution):
	"""solution = [num_iterations, vnd_iterations, vnd_attempts, perturbation, temperature_att]"""
	start_time = time.time()
	constructive_heuristic = Constructive_Heuristic.Constructive_Heuristic(instance)
	tree = constructive_heuristic.construct_tree_path()
	local_search = Local_Search.Local_Search(tree, instance, solution)
	local_search.local_search()
	full_time = time.time() - start_time
	return (tree.weight, full_time)


def get_average_solution(solution):
	value = 0
	time = 0
	for i in range(1):
		result = evaluate(solution)
		value += result[0]
		time += result[1]
	value = value/10.0
	time = time/10.0
	return (solution, value, time)


def get_best_solution_quality(best_solution):
	print("\n Qualidade:")
	print("Verificando iteracoes global")
	current_value = best_solution[0][0]
	current_parameters = best_solution[0].copy()
	for g_i in global_iterations:
		if g_i != current_value:
			current_parameters[0] = g_i
			result = get_average_solution(current_parameters)
			if result[1] < best_solution[1]:
				best_solution = result

	print("Verificando iteracoes VND")
	current_value = best_solution[0][1]
	current_parameters = best_solution[0].copy()
	for v_i in vnd_iterations:
		if v_i != current_value:
			current_parameters[1] = v_i
			result = get_average_solution(current_parameters)
			if result[1] < best_solution[1]:
				best_solution = result

	print("Verificando tentativas VND")
	current_value = best_solution[0][2]
	current_parameters = best_solution[0].copy()
	for v_a in vnd_attempts:
		if v_a != current_value:
			current_parameters[2] = v_a
			result = get_average_solution(current_parameters)
			if result[1] < best_solution[1]:
				best_solution = result

	print("Verificando perturbacao")
	current_value = best_solution[0][3]
	current_parameters = best_solution[0].copy()
	for p in perturbation:
		if p != current_value:
			current_parameters[3] = p
			result = get_average_solution(current_parameters)
			if result[1] < best_solution[1]:
				best_solution = result

	print("Verificando diminuicao de temperatura")
	current_value = best_solution[0][4]
	current_parameters = best_solution[0].copy()
	for t in temperature_att:
		if t != current_value:
			current_parameters[4] = t
			result = get_average_solution(current_parameters)
			if result[1] < best_solution[1]:
				best_solution = result

	return best_solution


def get_best_solution_time(best_solution, limit):
	print("\n Tempo:")
	print("Verificando iteracoes global")
	found_better = False

	current_value = best_solution[0][0]
	current_parameters = best_solution[0].copy()
	for g_i in global_iterations:
		if g_i != current_value:
			current_parameters[0] = g_i
			result = get_average_solution(current_parameters)
			if result[2] < best_solution[2]:
				if found_better:
					if result[1] <= best_solution[1]:
						best_solution = result
				else:
					if result[1] <= limit:
						best_solution = result

	print("Verificando iteracoes VND")
	current_value = best_solution[0][1]
	current_parameters = best_solution[0].copy()
	for v_i in vnd_iterations:
		if v_i != current_value:
			current_parameters[1] = v_i
			result = get_average_solution(current_parameters)
			if result[2] < best_solution[2]:
				if found_better:
					if result[1] <= best_solution[1]:
						best_solution = result
				else:
					if result[1] <= limit:
						best_solution = result

	print("Verificando tentativas VND")
	current_value = best_solution[0][2]
	current_parameters = best_solution[0].copy()
	for v_a in vnd_attempts:
		if v_a != current_value:
			current_parameters[2] = v_a
			result = get_average_solution(current_parameters)
			if result[2] < best_solution[2]:
				if found_better:
					if result[1] <= best_solution[1]:
						best_solution = result
				else:
					if result[1] <= limit:
						best_solution = result

	print("Verificando perturbacao")
	current_value = best_solution[0][3]
	current_parameters = best_solution[0].copy()
	for p in perturbation:
		if p != current_value:
			current_parameters[3] = p
			result = get_average_solution(current_parameters)
			if result[2] < best_solution[2]:
				if found_better:
					if result[1] <= best_solution[1]:
						best_solution = result
				else:
					if result[1] <= limit:
						best_solution = result

	print("Verificando diminuicao de temperatura")
	current_value = best_solution[0][4]
	current_parameters = best_solution[0].copy()
	for t in temperature_att:
		if t != current_value:
			current_parameters[4] = t
			result = get_average_solution(current_parameters)
			if result[2] < best_solution[2]:
				if found_better:
					if result[1] <= best_solution[1]:
						best_solution = result
				else:
					if result[1] <= limit:
						best_solution = result

	return best_solution


if __name__ == '__main__':
	best_solution = get_average_solution([100, 7, 4, 5, 0.99])
	print("Solucao inicial")
	print(best_solution)

	best_solution = get_best_solution_quality(best_solution)
	print("Melhor solucao de qualidade")
	print(best_solution)

	limit = best_solution[1] + best_solution[1]*0.1
	best_solution = get_best_solution_time(best_solution, limit)
	print("Melhor solucao tempo")
	print(best_solution)


	


	



