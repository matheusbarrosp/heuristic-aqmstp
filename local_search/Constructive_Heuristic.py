import Tree

class Constructive_Heuristic():

	def __init__(self, instance):
		self.instance = instance
		self.tree = Tree.Tree(instance.n)


	def adjacent_vertexes(self, vertex):	
		return self.tree.tree[vertex-1]


	def calculate_weight(self, vertex, vertex_in_tree):
		vertexes = self.adjacent_vertexes(vertex_in_tree)
		weight = self.instance.l_values[vertex-1][vertex_in_tree-1]
		for adjacent in vertexes:
			(i,j,k) = (vertex, vertex_in_tree, adjacent)
			weight += self.instance.q_values[i-1][j-1][k-1]
		return weight


	def find_best_vertex(self, corner, unused_vertexes):
		best_vertex = unused_vertexes[0]
		best_weight = self.calculate_weight(best_vertex, corner)
		for vertex in unused_vertexes[1:]:
			weight = self.calculate_weight(vertex, corner)
			if weight < best_weight:
				best_weight = weight
				best_vertex = vertex
		return (best_vertex, best_weight)


	def calculate_total_weight(self):
		weight = 0
		for i in range(1, self.instance.n + 1):
			for j in self.tree.tree[i-1]:
				if j > i:
					weight += self.instance.l_values[i-1][j-1]
				for k in self.tree.tree[j-1]:
					if k != i:
						weight += self.instance.q_values[i-1][j-1][k-1]
		return weight


	def construct_tree_path(self):
		initial_edge = (self.instance.smallest_linear_value[1], self.instance.smallest_linear_value[2])
		unused_vertexes = [i for i in range(1,self.instance.n + 1)]
		unused_vertexes.remove(initial_edge[0])
		unused_vertexes.remove(initial_edge[1])

		self.tree.tree[initial_edge[0]-1].append(initial_edge[1])
		self.tree.tree[initial_edge[1]-1].append(initial_edge[0])

		corners = [initial_edge[0], initial_edge[1]]
		i = len(unused_vertexes)

		while i > 0:
			option1 = self.find_best_vertex(corners[0], unused_vertexes)
			option2 = self.find_best_vertex(corners[1], unused_vertexes)
			choice = (option1[0], corners[0])
			if option1[1] > option2[1]:
				choice = (option2[0], corners[1])
				corners[1] = option2[0]
			else:
				corners[0] = option1[0]

			self.tree.tree[choice[0]-1].append(choice[1])
			self.tree.tree[choice[1]-1].append(choice[0])

			unused_vertexes.remove(choice[0])
			i -= 1

		self.tree.weight = self.calculate_total_weight()
		return self.tree


	def construct_tree_notpath(self):
		initial_edge = (self.instance.smallest_linear_value[1], self.instance.smallest_linear_value[2])
		unused_vertexes = [i for i in range(1,self.instance.n + 1)]
		unused_vertexes.remove(initial_edge[0])
		unused_vertexes.remove(initial_edge[1])

		used_vertexes = [initial_edge[0], initial_edge[1]]

		self.tree.tree[initial_edge[0]-1].append(initial_edge[1])
		self.tree.tree[initial_edge[1]-1].append(initial_edge[0])

		i = len(unused_vertexes)
		while i > 0:
			option = self.find_best_vertex_notpath(unused_vertexes, used_vertexes)
			self.tree.tree[option[0]-1].append(option[1])
			self.tree.tree[option[1]-1].append(option[0])

			unused_vertexes.remove(option[0])
			used_vertexes.append(option[0])

			i -= 1

		self.tree.weight = self.calculate_total_weight()
		return self.tree


	def find_best_vertex_notpath(self, unused_vertexes, used_vertexes):
		best_weight = -1
		best_vertex = -1
		chosen_inner = -1
		for used_vertex in used_vertexes:
			current_weight = 0
			for unused_vertex in unused_vertexes:
				current_weight = self.calculate_weight(unused_vertex, used_vertex)
				if current_weight < best_weight or best_weight == -1:
					best_weight = current_weight
					best_vertex = unused_vertex
					chosen_inner = used_vertex
		return best_vertex, chosen_inner

