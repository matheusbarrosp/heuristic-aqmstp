import sys
import Instance
import instance_reader
import Constructive_Heuristic
import Tree
import Local_Search
import time

if __name__ == '__main__':
	start_time = time.time()
	instance_file = sys.argv[1]
	instance = Instance.Instance(instance_reader.read_instance(instance_file))
	constructive_heuristic = Constructive_Heuristic.Constructive_Heuristic(instance)
	tree = constructive_heuristic.construct_tree_path()

	local_search = Local_Search.Local_Search(tree, instance, [50,6,5,2,0.97])
	local_search.local_search()
	full_time = time.time() - start_time
	tree.print()
	print(full_time)

