class Instance:
	def __init__(self, instance):
		self.l_values = instance[0]
		self.q_values = instance[1]
		self.n = instance[2]
		self.smallest_linear_value = instance[3]