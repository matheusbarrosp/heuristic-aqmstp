import random
import math
import sys

class Local_Search():
	def __init__(self, tree, instance, solution):
		self.tree = tree
		self.instance = instance
		self.number_of_iterations = solution[0]
		self.max_VND = solution[1]
		self.max_VND_attempts = solution[2]
		self.perturbation = solution[3]
		self.temperature_percentage = solution[4]


	def copy_tree(self, base_tree):
		new_tree = []
		for i in base_tree:
			new_tree.append(i.copy())
		return new_tree


	def remove_from_tree(self, removed):
		for r in removed:
			self.tree.weight -= self.instance.l_values[r[0]-1][r[1]-1]
			self.tree.tree[r[0]-1].remove(r[1])
			self.tree.tree[r[1]-1].remove(r[0])
			for a in self.tree.tree[r[0]-1]:
				self.tree.weight -= 2*self.instance.q_values[r[1]-1][r[0]-1][a-1]
			for a in self.tree.tree[r[1]-1]:
				self.tree.weight -= 2*self.instance.q_values[r[0]-1][r[1]-1][a-1]


	def insert_in_tree(self, inserted):
		for i in inserted:
			self.tree.weight += self.instance.l_values[i[0]-1][i[1]-1]
			for a in self.tree.tree[i[0]-1]:
				self.tree.weight += 2*self.instance.q_values[i[1]-1][i[0]-1][a-1]
			for a in self.tree.tree[i[1]-1]:
				self.tree.weight += 2*self.instance.q_values[i[0]-1][i[1]-1][a-1]
			self.tree.tree[i[0]-1].append(i[1])
			self.tree.tree[i[1]-1].append(i[0])


	def local_search(self):
		best_tree = self.copy_tree(self.tree.tree)
		best_weight = self.tree.weight
		
		current_tree = self.copy_tree(self.tree.tree)
		current_weight = self.tree.weight
		T = 950
		perturbation = self.perturbation
		for i in range(1, self.number_of_iterations):
			#print("Iteracao {0}".format(i))
			#print("Inicio: {0}".format(self.tree.weight))
			self.vnd()
			#print("VND: {0}".format(self.tree.weight))
			new_weight = self.tree.weight
			if new_weight < best_weight:
				best_tree = self.copy_tree(self.tree.tree)
				best_weight = new_weight

			delta = new_weight - current_weight
			if delta < 0:
				current_tree = self.copy_tree(self.tree.tree)
				current_weight = new_weight
			else:
				probability = 1/math.exp(delta/T)
				random_number = random.uniform(0,1)
				if random_number < probability:
					current_tree = self.copy_tree(self.tree.tree)
					current_weight = new_weight
			self.tree.tree = self.copy_tree(current_tree)
			self.tree.weight = current_weight

			self.perturbate(perturbation)
			T *= self.temperature_percentage

		self.tree.tree = best_tree
		self.tree.weight = best_weight


	def unite_lists(self, forest, i, j):
		l1 = forest[i]
		l2 = forest[j]
		for element in l2:
			if element not in l1:
				l1.append(element)


	def perturbate(self, perturbation):
		removed_edges = []
		for i in range(perturbation):
			removed1 = random.randint(1, self.instance.n)
			removed2 = random.choice(self.tree.tree[removed1-1])
			if removed1 < removed2:
				removed = (removed1, removed2)
			else:
				removed = (removed2, removed1)
			while removed in removed_edges:
				removed1 = random.randint(1, self.instance.n)
				removed2 = random.choice(self.tree.tree[removed1-1])
				if removed1 < removed2:
					removed = (removed1, removed2)
				else:
					removed = (removed2, removed1)
			removed_edges.append(removed)
		self.reconstruct_tree_random(removed_edges)


	def reconstruct_tree_random(self, removed_edges):
		self.remove_from_tree(removed_edges)
		
		forest = self.find_forest(removed_edges)
		forest_size = len(forest)
		inserted = []
		while forest_size > 1:
			pos1 = random.randint(0, len(forest)-1)
			pos2 = random.randint(0, len(forest)-1)
			while pos2 == pos1:
				pos2 = random.randint(0, len(forest)-1)
			edge1 = random.choice(forest[pos1])
			edge2 = random.choice(forest[pos2])
			if edge1 < edge2:
				connection = (edge1, edge2)
			else:
				connection = (edge2, edge1)

			inserted.append(connection)
			self.unite_lists(forest, pos1, pos2)
			forest.pop(pos2)
			forest_size -= 1

		self.insert_in_tree(inserted)


	def vnd(self):
		i = 1
		while i <= self.max_VND:
			removed_options = [] 
			better_option = None
			j = 0
			better_found = False
			while j < i*self.max_VND_attempts and not better_found:
				removed_edges = []
				while removed_edges == []:
					for k in range(i):
						choice1 = random.randint(1, self.instance.n)
						choice2 = random.choice(self.tree.tree[choice1-1])
						if choice1 < choice2:
							choice = (choice1, choice2)
						else:
							choice = (choice2, choice1)
						while choice in removed_edges:
							choice1 = random.randint(1, self.instance.n)
							choice2 = random.choice(self.tree.tree[choice1-1])
							if choice1 < choice2:
								choice = (choice1, choice2)
							else:  
								choice = (choice2, choice1)
						removed_edges.append(choice)
					if removed_edges in removed_options:
						removed_edges = []

				removed_options.append(removed_edges)
				old_weight = self.tree.weight
				self.remove_from_tree(removed_edges)
				if i <= 2:
					better_option = self.reconstruct_tree_first_random_possibility(removed_edges, old_weight)
				else:
					better_option = self.reconstruct_tree_greed(removed_edges, old_weight)

				if better_option != -1:
					better_found = True
				else:
					self.insert_in_tree(removed_edges)
				j += 1

			if better_found:
				i = 1
			else:
				i += 1


	def reconstruct_tree_greed(self, removed_edges, old_weight):
		forest = self.find_forest(removed_edges)
		counter = len(forest)

		connections = [[] for i in range(counter)]
		for i in range(counter):
			connections[i] = [(sys.maxsize, -1, -1) for x in range(counter)]

		for i in range(counter-1):
				for v1 in forest[i]:
					for j in range(i+1, counter):
						for v2 in forest[j]:
							if v1 < v2:
								new_edge = (v1,v2)
							else:
								new_edge = (v2,v1)
							current_weight = self.instance.l_values[new_edge[0]-1][new_edge[1]-1] + self.calculate_quadratic_weight(new_edge[0], new_edge[1]) + self.calculate_quadratic_weight(new_edge[1], new_edge[0])
							if current_weight < connections[i][j][0]:
								connections[i][j] = (current_weight, (i, j), (v1, v2))
								connections[j][i] = (current_weight, (i, j), (v1, v2))
		elements = []
		for i in range(counter-1):
			for j in range(i+1, counter):
				elements.append(connections[i][j])

		elements.sort(key=lambda tup: tup[0])

		connections = []
		sets = [[i] for i in range(counter)]
		for element in elements:
			if not element[1][0] in sets[element[1][1]]:
				if element[2][0] < element[2][1]:
					connections.append((element[2][0], element[2][1]))
				else:
					connections.append((element[2][1], element[2][0]))
				for x in sets[element[1][1]]:
					if not x in sets[element[1][0]]:
						sets[element[1][0]].append(x)

				for x in sets[element[1][0]]:
					sets[x] = sets[element[1][0]]
				counter -= 1
			if counter == 1:
				break
		self.insert_in_tree(connections)
		
		if self.tree.weight < old_weight:
			return self.tree.weight
		else:
			self.remove_from_tree(connections)
			return -1


	def calculate_quadratic_weight(self, vertex, vertex_in_tree):
		vertexes = self.tree.tree[vertex_in_tree-1]
		weight = 0
		for adjacent in vertexes:
			weight += self.instance.q_values[vertex-1][vertex_in_tree-1][adjacent-1]
		return weight


	def reconstruct_tree_first_random_possibility(self, removed_edges, old_weight):		
		forest = self.find_forest(removed_edges)
		possibilities = []
		if len(removed_edges) == 1:
			for v1 in forest[0]:
				for v2 in forest[1]:
					if v1 < v2:
						possibilities.append((v1, v2))
					else:
						possibilities.append((v2, v1))
		else:
			for v1 in forest[0]:
				for v2 in forest[1]:
					for v3 in forest[2]:
						if v1 < v2 and v1 < v3:
							possibilities.append((v1,v2,v1,v3))
						elif v1 < v2 and v1 > v3:
							possibilities.append((v1,v2,v3,v1))
						elif v1 > v2 and v1 < v3:
							possibilities.append((v2,v1,v1,v3))
						else:
							possibilities.append((v2,v1,v3,v1))
		return self.connect_first_possibility(possibilities, removed_edges, old_weight)


	def connect_first_possibility(self, possibilities, removed, old_weight):
		double_connection = False
		if len(possibilities[0]) == 4:
			double_connection = True

		while len(possibilities) > 0:
			possibility = random.choice(possibilities)
			if double_connection:
				inserted = [(possibility[0], possibility[1]), (possibility[2], possibility[3])]
			else:
				inserted = [possibility]

			self.insert_in_tree(inserted)

			if self.tree.weight < old_weight:
				return self.tree.weight
			else:
				possibilities.remove(possibility)
				self.remove_from_tree(inserted)
		return -1


	def find_forest(self, removed_edges):
		forest = []
		forest.append(self.get_connected(removed_edges[0][0]))
		forest.append(self.get_connected(removed_edges[0][1]))
		for edge in removed_edges[1:]:
			for vertex in edge:
				contains = False
				for f in forest:
					if vertex in f:
						contains = True
				if not contains:
					forest.append(self.get_connected(vertex))

		return forest


	def get_connected(self,  v):
		visited_vertexes = []
		stack = [v]
		while len(stack) > 0:
			current = stack.pop()
			visited_vertexes.append(current)
			for vertex in self.tree.tree[current-1]:
				if not vertex in visited_vertexes:
					stack.append(vertex)
		return visited_vertexes


	"""
	def initialize_connections(self, forest):
		counter = len(forest)
		connections = []
		for i in range(counter-1):
			for v1 in forest[i]:
				for j in range(i+1, counter):
					for v2 in forest[j]:
						if v1 < v2:
							new_edge = (v1,v2)
						else:
							new_edge = (v2,v1)
						current_weight = self.instance.l_values[new_edge] + self.calculate_quadratic_weight(new_edge[0], new_edge[1]) + self.calculate_quadratic_weight(new_edge[1], new_edge[0])
						connections.append((new_edge,current_weight,(i,j)))
						if len(connections) == 3:
							if connections[0][1] > connections[1][1]:
								tmp = connections[0]
								connections[0] = connections[1]
								connections[1] = tmp
							if connections[1][1] > connections[2][1]:
								tmp = connections[1]
								connections[1] = connections[2]
								connections[2] = tmp
							if connections[0][1] > connections[1][1]:
								tmp = connections[0]
								connections[0] = connections[1]
								connections[1] = tmp
							
							return connections


	def reconstruct_tree_greed(self, removed_edges):
		forest = self.find_forest(removed_edges)
		counter = len(forest)
		inserted = []
		while counter > 1:
			iteration = 0
			best_connections = self.initialize_connections(forest)
			for i in range(counter-1):
				for v1 in forest[i]:
					for j in range(i+1, counter):
						for v2 in forest[j]:
							if iteration > 2:
								if v1 < v2:
									new_edge = (v1,v2)
								else:
									new_edge = (v2,v1)
								current_weight = self.instance.l_values[new_edge] + self.calculate_quadratic_weight(new_edge[0], new_edge[1]) + self.calculate_quadratic_weight(new_edge[1], new_edge[0])
								for k in range(3)[-1::-1]:
									if current_weight < best_connections[k][1]:
										best_connections[k] = (new_edge, current_weight, (i,j))
										aux = k
										while aux > 0:
											if best_connections[aux-1][1] > best_connections[aux][1]:
												tmp = best_connections[aux-1]
												best_connections[aux-1] = best_connections[aux]
												best_connections[aux] = tmp
												aux -= 1
											else:
												aux = 0
										break
							else:
								iteration += 1

			choice = random.choice(best_connections)
			self.tree.tree[choice[0][0]-1].append(choice[0][1])
			self.tree.tree[choice[0][1]-1].append(choice[0][0])
			inserted.append(choice[0])

			self.unite_lists(forest, choice[2][0], choice[2][1])
			forest.pop(choice[2][1])
			counter -= 1
		new_weight = self.recalculate_tree_weight(inserted, removed_edges)
		if new_weight < self.tree.weight:
			return new_weight
		else:
			#Remover os inseridos
			for edge in inserted:
				self.tree.tree[edge[0]-1].remove(edge[1])
				self.tree.tree[edge[1]-1].remove(edge[0])
			return -1
	"""


	"""
	def change_path(self, removed_edge1, removed_edge2):
		aux_tree = self.copy_tree(self.tree.tree)

		aux_tree[removed_edge1[0]-1].remove(removed_edge1[1])
		aux_tree[removed_edge1[1]-1].remove(removed_edge1[0])
		aux_tree[removed_edge2[0]-1].remove(removed_edge2[1])
		aux_tree[removed_edge2[1]-1].remove(removed_edge2[0])

		if removed_edge1[0] < removed_edge2[0]:
			added1 = (removed_edge1[0], removed_edge2[0])
		else:
			added1 = (removed_edge2[0], removed_edge1[0])
		aux_tree[added1[0]-1].append(added1[1])
		aux_tree[added1[1]-1].append(added1[0])
		if removed_edge1[1] < removed_edge2[1]:
			added2 = (removed_edge1[1], removed_edge2[1])
		else:
			added2 = (removed_edge2[1], removed_edge1[1])
		aux_tree[added2[0]-1].append(added2[1])
		aux_tree[added2[1]-1].append(added2[0])

		if self.find_cycle(aux_tree) or not self.is_connected(aux_tree):
			aux_tree[added1[0]-1].remove(added1[1])
			aux_tree[added1[1]-1].remove(added1[0])
			aux_tree[added2[0]-1].remove(added2[1])
			aux_tree[added2[1]-1].remove(added2[0])

			if removed_edge1[0] < removed_edge2[1]:
				added1 = (removed_edge1[0], removed_edge2[1])
			else:
				added1 = (removed_edge2[1], removed_edge1[0])
			aux_tree[added1[0]-1].append(added1[1])
			aux_tree[added1[1]-1].append(added1[0])
			if removed_edge1[1] < removed_edge2[0]:
				added2 = (removed_edge1[1], removed_edge2[0])
			else:
				added2 = (removed_edge2[0], removed_edge1[1])
			aux_tree[added2[0]-1].append(added2[1])
			aux_tree[added2[1]-1].append(added2[0])
		removed = [removed_edge1, removed_edge2]
		added = [added1, added2]
		weight = self.recalculate_tree_weight(added, removed)
		return (aux_tree, weight)


	def local_search_tsp_min(self):
		change = True
		size = self.instance.n - 1
		while change:
			change = False
			possible_changes = []
			for i in range(1, size):
				for j in self.tree.tree[i-1]:
					if j > i:
						for k in range(i+1, size+1):
							if k != j:
								for l in self.tree.tree[k-1]:
									if l > k:
										if i != k and i != l and j != k and j != l:
											possible_changes.append(((i,j),(k,l)))
			best_tree = self.copy_tree(self.tree.tree)
			best_weight = self.tree.weight
			for possibility in possible_changes:
				new_tree = self.change_path(possibility[0], possibility[1])
				current_weight = new_tree[1]
				if current_weight < best_weight:
					best_weight = current_weight
					best_tree = new_tree[0]
					change = True
			self.tree.tree = best_tree
			self.tree.weight = best_weight


	def find_cycle(self, tree):
		visited_vertexes = []
		stack = [(1,0)]
		previous = 0
		while len(stack) > 0:
			current = stack.pop()
			if current[0] in visited_vertexes:
				return True
			else:
				visited_vertexes.append(current[0])
				previous = current[1]
			for vertex in tree[current[0]-1]:
				if vertex == current[0]:
					return True
				if vertex != previous:
					stack.append((vertex, current[0]))
		return False


	def is_connected(self, tree):
		visited_vertexes = set()
		stack = [1]
		while len(stack) > 0:
			current = stack.pop()
			visited_vertexes.add(current)
			for vertex in tree[current-1]:
				if not vertex in visited_vertexes:
					stack.append(vertex)
		return len(visited_vertexes) == self.instance.n
	"""
