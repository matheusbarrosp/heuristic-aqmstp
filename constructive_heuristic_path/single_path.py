import instance_reader
import sys
from collections import OrderedDict

class Instance:
	def __init__(self, instance):
		self.l_values = self.__create_ordered_dict(instance[0])
		self.q_values = instance[1]
		self.n = instance[2]


	def __create_ordered_dict(self, dictionary):
		return OrderedDict(sorted(dictionary.items(), key=lambda x: x[1]))


class Tree:
	def __init__(self, instance):
		self.instance = instance
		self.tree = []


	def adjacent_vertexes(self, vertex):	
		vertexes = set()
		for edge in self.tree:
			if vertex in edge:
				if edge[0] != vertex:
					vertexes.add(edge[0])
				else:
					vertexes.add(edge[1])
		return vertexes


	def calculate_weight(self, vertex, vertex_in_tree):
		#print("Calculando peso entre ({0},{1})".format(vertex, vertex_in_tree))
		vertexes = self.adjacent_vertexes(vertex_in_tree)
		#print("Adjacentes a {0}: {1}".format(vertex_in_tree, vertexes))
		a = vertex
		b = vertex_in_tree
		if vertex > vertex_in_tree:
			a = vertex_in_tree
			b = vertex
		weight = self.instance.l_values[(a,b)]
		#print("Peso linear: {0}".format(weight))
		for adjacent in vertexes:
			x = adjacent
			y = vertex_in_tree
			if adjacent > vertex_in_tree:
				x = vertex_in_tree
				y = adjacent
			#print("Arestas: ({0},{1},{2},{3})".format(a,b,x,y))
			if (a,b,x,y) in self.instance.q_values:
				#print("Peso {0}".format(self.instance.q_values[(a,b,x,y)]))
				weight += self.instance.q_values[(a,b,x,y)]
		return weight


	def find_best_vertex(self, corner, unused_vertexes):
		#print("Achar melhor para {0}".format(corner))
		best_vertex = unused_vertexes[0]
		best_weight = self.calculate_weight(best_vertex, corner)
		#print("Melhor inicial: {0}".format(best_vertex))
		#print("Melhor peso: {0}".format(best_weight))
		for vertex in unused_vertexes[1:]:
			weight = self.calculate_weight(vertex, corner)
			#print("Atual: {0}\nPeso:{1}".format(vertex, weight))
			if weight < best_weight:
				best_weight = weight
				best_vertex = vertex
		return best_vertex


	def create_tree(self):
		initial_edge = list(self.instance.l_values.keys())[0]
		#print("Inicial: {0}".format(initial_edge))

		unused_vertexes = [i for i in range(1,self.instance.n + 1)]
		unused_vertexes.remove(initial_edge[0])
		unused_vertexes.remove(initial_edge[1])
		#print("Unused vertexes: {0}".format(unused_vertexes))

		self.tree.append(initial_edge)
		#print("Tree: {0}".format(self.tree))
		corner = initial_edge[1]
		i = len(unused_vertexes)

		while i > 0:
			best_vertex = self.find_best_vertex(corner, unused_vertexes)
			#print("Best vertex: {0}".format(best_vertex))
			best_edge = (best_vertex, corner)
			corner = best_vertex

			if best_edge in self.instance.l_values:
				self.tree.append(best_edge)
			else:
				self.tree.append((best_edge[1], best_edge[0]))

			unused_vertexes.remove(best_vertex)
			#print(self.tree)
			#print(unused_vertexes)
			i -= 1


	def calculate_total_weight(self):
		weight = 0
		for edge1 in self.tree:
			weight += self.instance.l_values[edge1]
			for edge2 in self.tree:
				if edge1 != edge2:
					if (edge1[0], edge1[1], edge2[0], edge2[1]) in self.instance.q_values:
						weight += self.instance.q_values[(edge1[0], edge1[1], edge2[0], edge2[1])]

		return weight


	def print(self):
		print(self.tree)
		print(self.calculate_total_weight())


if __name__ == '__main__':
	file = sys.argv[1]
	instance = instance_reader.read_instance(file)
	tree = Tree(Instance(instance))

	tree.create_tree()
	tree.print()