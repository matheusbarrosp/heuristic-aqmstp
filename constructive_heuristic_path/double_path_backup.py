import instance_reader
import sys
from collections import OrderedDict
import heapq

def create_sorted_dict(dictionary):
	return OrderedDict(sorted(dictionary.items(), key=lambda x: x[1]))


def create_heap(edge, n, l_values, q_values):
	heap = []
	for x in range (1,n+1):
		if x not in edge:
			if (edge[1],x) in l_values:
				c = l_values[(edge[1], x)] + q_values[(edge[1], x, edge[0], edge[1])]
				heap.append([c,x,edge[1]])
			else:
				c = l_values[(x,edge[1])] + q_values[(x, edge[1], edge[0], edge[1])]
				heap.append([c,x,edge[1]])

	heapq.heapify(heap)
	return heap


def adjacent_values(edge, tree, q_values):
	#print("Adjacent values of {0}".format(edge))
	sum = 0
	for e in tree:
		if (edge[0], edge[1], e[0], e[1]) in q_values:
			#print("adjacent to {0}: {1}".format((e[0],e[1]), q_values[(edge[0], edge[1], e[0], e[1])]))
			sum += q_values[(edge[0], edge[1], e[0], e[1])]
	#print("Sum: {0}".format(sum))
	return sum


def is_in_tree(vertex, tree):
	for edge in tree:
		if vertex == edge[0] or vertex == edge[1]:
			return True
	return False


def update_heap(heap, tree, l_values, q_values):
	#print("Updating heap")
	edge = tree[-1]
	for vertex in edge:
		if not is_in_tree(vertex,tree[:-1]):
			for x in heap:
				#print("x: {0}, Edge: {1}".format(x,x[1]))
				if (vertex,x[1]) in l_values:
					c = l_values[(vertex, x[1])] + adjacent_values((vertex, x[1]), tree, q_values)
				else:
					c = l_values[(x[1],vertex)] + adjacent_values((x[1], vertex), tree, q_values)
				#print("c = {0}".format(c))
				if c < x[0] or x[2] != vertex:
					#print("{0} entrou na condicao".format(edge))
					x[0] = c
					x[2] = vertex
	heapq.heapify(heap)


def create_tree(l_values, q_values, n):
	initial_edge = list(l_values.keys())[0]
	heap = create_heap(initial_edge, n, l_values, q_values)
	tree = []
	tree.append(initial_edge)

	#print("Tree")
	#print(tree)
	#print("Heap")
	#print(heap)
	#print("\n")
	
	while len(heap) != 0:
		e = heapq.heappop(heap)
		if (e[1], e[2]) in l_values:
			tree.append((e[1],e[2]))
		else:
			tree.append((e[2], e[1]))
		update_heap(heap, tree, l_values, q_values)
		#print("Tree")
		#print(tree)
		#print("Heap")
		#print(heap)
		#print("\n")
	
	return tree


def calculate_weight(tree, l_values, q_values):
	weight = 0
	for edge1 in tree:
		weight += l_values[edge1]
		for edge2 in tree:
			if edge1 != edge2:
				if (edge1[0], edge1[1], edge2[0], edge2[1]) in q_values:
					weight += q_values[(edge1[0], edge1[1], edge2[0], edge2[1])]

	return weight


if __name__ == '__main__':

	file = sys.argv[1]

	instance = instance_reader.read_instance(file)
	l_values = instance[0]
	q_values = instance[1]
	n = instance[2]

	#create_graph(l_values, q_values)
	l_values = create_sorted_dict(l_values)

	tree = create_tree(l_values, q_values, n)
	print(tree)
	print(calculate_weight(tree, l_values, q_values))