import random
import math
import sys

class IG():
	def __init__(self, tree, instance):
		self.tree = tree
		self.instance = instance


	def copy_tree(self, base_tree):
		new_tree = []
		for i in base_tree:
			new_tree.append(i.copy())
		return new_tree

	
	def remove_from_tree(self, removed):
		for r in removed:
			self.tree.weight -= self.instance.l_values[r[0]-1][r[1]-1]
			self.tree.tree[r[0]-1].remove(r[1])
			self.tree.tree[r[1]-1].remove(r[0])
			for a in self.tree.tree[r[0]-1]:
				self.tree.weight -= 2*self.instance.q_values[r[1]-1][r[0]-1][a-1]
			for a in self.tree.tree[r[1]-1]:
				self.tree.weight -= 2*self.instance.q_values[r[0]-1][r[1]-1][a-1]


	def insert_in_tree(self, inserted):
		for i in inserted:
			self.tree.weight += self.instance.l_values[i[0]-1][i[1]-1]
			for a in self.tree.tree[i[0]-1]:
				self.tree.weight += 2*self.instance.q_values[i[1]-1][i[0]-1][a-1]
			for a in self.tree.tree[i[1]-1]:
				self.tree.weight += 2*self.instance.q_values[i[0]-1][i[1]-1][a-1]
			self.tree.tree[i[0]-1].append(i[1])
			self.tree.tree[i[1]-1].append(i[0])


	def destruction_phase(self, remove_number):
		removed_edges = []
		while len(removed_edges) < remove_number:
			repeated = True
			while repeated:
				choice1 = random.randint(1, self.instance.n)
				choice2 = random.choice(self.tree.tree[choice1-1])
				if choice1 < choice2:
					choice = (choice1, choice2)
				else:  
					choice = (choice2, choice1)
				if choice not in removed_edges:
					repeated = False
			removed_edges.append(choice)
			
		#old_weight = self.tree.weight
		self.remove_from_tree(removed_edges)
		return removed_edges


	def construction_phase(self, removed_edges):
		forest = self.find_forest(removed_edges)
		counter = len(forest)

		connections = [[] for i in range(counter)]
		for i in range(counter):
			connections[i] = [(sys.maxsize, -1, -1) for x in range(counter)]

		for i in range(counter-1):
				for v1 in forest[i]:
					for j in range(i+1, counter):
						for v2 in forest[j]:
							if v1 < v2:
								new_edge = (v1,v2)
							else:
								new_edge = (v2,v1)
							current_weight = self.instance.l_values[new_edge[0]-1][new_edge[1]-1] + self.calculate_quadratic_weight(new_edge[0], new_edge[1]) + self.calculate_quadratic_weight(new_edge[1], new_edge[0])
							if current_weight < connections[i][j][0]:
								connections[i][j] = (current_weight, (i, j), (v1, v2))
								connections[j][i] = (current_weight, (i, j), (v1, v2))
		elements = []
		for i in range(counter-1):
			for j in range(i+1, counter):
				elements.append(connections[i][j])
		elements.sort(key=lambda tup: tup[0])
		connections = []
		sets = [[i] for i in range(counter)]
		for element in elements:
			if not element[1][0] in sets[element[1][1]]:
				if element[2][0] < element[2][1]:
					connections.append((element[2][0], element[2][1]))
				else:
					connections.append((element[2][1], element[2][0]))
				for x in sets[element[1][1]]:
					if not x in sets[element[1][0]]:
						sets[element[1][0]].append(x)

				for x in sets[element[1][0]]:
					sets[x] = sets[element[1][0]]
				counter -= 1
			if counter == 1:
				break
		self.insert_in_tree(connections)
	

	def best_connection(self, forest, removed_edge):
		best_weight = sys.maxsize
		best_connection = (-1,-1)
		for v1 in forest[0]:
			for v2 in forest[1]:
				if v1 < v2:
					new_edge = (v1,v2)
				else:
					new_edge = (v2,v1)
				current_weight = self.instance.l_values[new_edge[0]-1][new_edge[1]-1] + self.calculate_quadratic_weight(new_edge[0], new_edge[1]) + self.calculate_quadratic_weight(new_edge[1], new_edge[0])
				if current_weight < best_weight:
					best_weight = current_weight
					best_connection = new_edge

		return (best_connection, best_weight)


	def abc_local_search(self):
		#print("ABC")
		local_min = False
		while not local_min:
			choice1 = random.randint(1, self.instance.n)
			choice2 = random.choice(self.tree.tree[choice1-1])
			if choice1 < choice2:
				choice = (choice1, choice2)
			else:  
				choice = (choice2, choice1)

			old_weight = self.tree.weight
			self.remove_from_tree([choice])
			forest = self.find_forest([choice])
			edge, weight = self.best_connection(forest, choice)
			if weight + self.tree.weight < old_weight:
				#print("Found better: {0} , {1}".format(edge,weight))
				self.insert_in_tree([edge])
			else:
				local_min = True
				self.insert_in_tree([choice])



	def ig(self):
		best_tree = self.copy_tree(self.tree.tree)
		best_weight = self.tree.weight
		#print("Initial best weight: {0}".format(best_weight))
		i = 0
		#remove_number = int(self.instance.n * 0.6)
		remove_number = int(self.instance.n * 0.1)
		#melhor = 0
		#pior = 0
		while i < 100:
			#print("\tIteration: {0}".format(i))
			#original = self.tree.weight
			#print(i, original)
			removed_edges = self.destruction_phase(remove_number)
			self.construction_phase(removed_edges)
			#construido = self.tree.weight
			#print("Reconstructed weight: {0}".format(self.tree.weight))
			self.abc_local_search()
			#local = self.tree.weight
			#print('ABC weight: {0}'.format(self.tree.weight))
			if self.tree.weight < best_weight:
				best_tree = self.copy_tree(self.tree.tree)
				best_weight = self.tree.weight
				#print("Original ", original)
				#print("Construido ", construido)
				#print('Local ', local)
				#print("New best weight: {0}".format(best_weight))
				#melhor += 1
			#else:
				#self.tree.tree = self.copy_tree(best_tree)
				#self.tree.weight = best_weight
				#pior += 1
			i += 1

		self.tree.tree = best_tree
		self.tree.weight = best_weight

		#print((melhor, pior))
		return self.tree

	def find_forest(self, removed_edges):
		forest = []
		forest.append(self.get_connected(removed_edges[0][0]))
		forest.append(self.get_connected(removed_edges[0][1]))
		for edge in removed_edges[1:]:
			for vertex in edge:
				contains = False
				for f in forest:
					if vertex in f:
						contains = True
				if not contains:
					forest.append(self.get_connected(vertex))

		return forest


	def get_connected(self,  v):
		visited_vertexes = []
		stack = [v]
		while len(stack) > 0:
			current = stack.pop()
			visited_vertexes.append(current)
			for vertex in self.tree.tree[current-1]:
				if not vertex in visited_vertexes:
					stack.append(vertex)
		return visited_vertexes


	def calculate_quadratic_weight(self, vertex, vertex_in_tree):
		vertexes = self.tree.tree[vertex_in_tree-1]
		weight = 0
		for adjacent in vertexes:
			weight += 2*self.instance.q_values[vertex-1][vertex_in_tree-1][adjacent-1]
		return weight