class Tree():
	def __init__(self, instance, ig=True):
		self.tree = [[] for i in range(instance.n)]
		if ig:
			for i in range(instance.n):
				for j in range(1,instance.n+1):
					if j != i+1:
						self.tree[i].append(j)
			self.weight = self.calculate_total_weight(instance)
		else:
			self.weight = 0



	def calculate_total_weight(self, instance):
		weight = 0
		for i in range(1, instance.n + 1):
			for j in self.tree[i-1]:
				if j > i:
					weight += instance.l_values[i-1][j-1]
				for k in self.tree[j-1]:
					if k != i:
						weight += instance.q_values[i-1][j-1][k-1]
		return weight


	def __str__(self):
		output = ""
		#for i in range (1, len(self.tree)+1):
		#	output += "({0}: {1}), ".format(i, self.tree[i-1])
		#output += "\n"
		output += "{0}".format(self.weight)
		return output
