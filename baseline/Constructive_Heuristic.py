import Tree
import sys

class Constructive_Heuristic():

	def __init__(self, instance, ig=True):
		self.instance = instance
		self.tree = Tree.Tree(instance, ig)


	def adjacent_vertexes(self, vertex):	
		return self.tree.tree[vertex-1]


	def initialize_weights(self):
		edges = {}
		for edge in self.instance.edges:
			l_weight = self.instance.l_values[edge[0]-1][edge[1]-1]
			q_weight = 0
			for a in self.adjacent_vertexes(edge[0]):
				if a != edge[1]:
					q_weight += 2*self.instance.q_values[edge[1]-1][edge[0]-1][a-1]
			for a in self.adjacent_vertexes(edge[1]):
				if a != edge[0]:
					q_weight += 2*self.instance.q_values[edge[0]-1][edge[1]-1][a-1]
			edges[edge] = l_weight + q_weight
		return edges


	def will_disconnect(self, edge):
		reach = 0
		visited = [False for i in range(self.instance.n)]
		stack = [edge[0]]
		visited[edge[0]-1] = True
		while stack:
			v = stack.pop()
			for a in self.adjacent_vertexes(v):
				if visited[a-1] == False:
					if (v,a) != edge and (a,v) != edge:
						stack.append(a)
						visited[a-1] = True
						reach += 1
		return not reach == (self.instance.n - 1)


	def update_weights(self, edges, edge):
		for a in self.adjacent_vertexes(edge[0]):
			if edge[0] < a:
				new_weight = edges[(edge[0], a)] - 2*self.instance.q_values[a-1][edge[0]-1][edge[1]-1]
				edges[(edge[0], a)] = new_weight
			else:
				new_weight = edges[(a,edge[0])] - 2*self.instance.q_values[a-1][edge[0]-1][edge[1]-1]
				edges[(a, edge[0])] = new_weight
		for a in self.adjacent_vertexes(edge[1]):
			if edge[1] < a:
				new_weight = edges[(edge[1], a)] - 2*self.instance.q_values[a-1][edge[1]-1][edge[0]-1]
				edges[(edge[1], a)] = new_weight
			else:
				new_weight = edges[(a,edge[1])] - 2*self.instance.q_values[a-1][edge[1]-1][edge[0]-1]
				edges[(a, edge[1])] = new_weight


	def destructive_method(self):
		edges = self.initialize_weights()
		r = len(self.instance.edges)
		while r >= self.instance.n:
			edge = max(edges.keys(), key=(lambda k: edges[k]))
			if not self.will_disconnect(edge):
				weight = edges.pop(edge)
				# removes edge from tree
				self.tree.tree[edge[0]-1].remove(edge[1])
				self.tree.tree[edge[1]-1].remove(edge[0])
				# update edges weights
				self.update_weights(edges, edge)
				self.tree.weight -= weight
				r -= 1	
			else:
				edges[edge] = 0
		return self.tree


	def remove_cycles(self, edges, edge, components):
		component = components[edge[0]-1]
		vertices = []
		for i in range(len(components)):
			if components[i] == component:
				vertices.append(i+1)
		removed = []
		for v1 in vertices:
			for v2 in vertices:
				if v1 < v2:
					if (v1,v2) in edges:
						edges.pop((v1,v2))
						removed.append((v1,v2))
				else:
					if (v2,v1) in edges:
						edges.pop((v2,v1))
						removed.append((v2,v1))

		return removed


	def update_components(self, edge, components):
		c1 = components[edge[0]-1]
		c2 = components[edge[1]-1]
		if c1 < c2:
			for i in range(len(components)):
				if components[i] == c2:
					components[i] = c1
		else:
			for i in range(len(components)):
				if components[i] == c1:
					components[i] = c2


	def calculate_out_contribution(self, removed_edges, edge):
		q_weight = 0
		for r in removed_edges:
			if edge[0] == r[0]:
				q_weight += 2*self.instance.q_values[edge[1]-1][edge[0]-1][r[1]-1]
			elif edge[0] == r[1]:
				q_weight += 2*self.instance.q_values[edge[1]-1][edge[0]-1][r[0]-1]
			elif edge[1] == r[0]:
				q_weight += 2*self.instance.q_values[edge[0]-1][edge[1]-1][r[1]-1]
			elif edge[1] == r[1]:
				q_weight += 2*self.instance.q_values[edge[0]-1][edge[1]-1][r[0]-1]
		return q_weight


	def update_weights_h2(self, inserted_edges, available_edges, new_edge, removed_edges):
		for edge in available_edges:
			linear = available_edges[edge][1]
			if edge[0] in new_edge or edge[1] in new_edge:
				if edge[0] == new_edge[0]:
					a = edge[1]
					b = edge[0]
					c = new_edge[1]
				elif edge[0] == new_edge[1]:
					a = edge[1]
					b = edge[0]
					c = new_edge[0]
				elif edge[1] == new_edge[0]:
					a = edge[0]
					b = edge[1]
					c = new_edge[1]
				elif edge[1] == new_edge[1]:
					a = edge[0]
					b = edge[1]
					c = new_edge[0]
				tree_contribution = available_edges[edge][2] + 2*(self.instance.q_values[a-1][b-1][c-1])
			else:
				tree_contribution = available_edges[edge][2]
				
			out_contribution = available_edges[edge][3] - self.calculate_out_contribution(removed_edges, edge)
			total = linear + tree_contribution + (((self.instance.n-1-inserted_edges)/(len(available_edges)-1))*out_contribution)
			available_edges[edge] = (total, linear, tree_contribution, out_contribution)			


	def initialize_contributions_h2(self):
		edges = {}
		vertices = [i for i in range(1,self.instance.n+1)]
		for edge in self.instance.edges:
			l_weight = self.instance.l_values[edge[0]-1][edge[1]-1]
			q_weight = 0
			for a in vertices:
				if a not in edge:
					q_weight += 2*self.instance.q_values[edge[1]-1][edge[0]-1][a-1]
					q_weight += 2*self.instance.q_values[edge[0]-1][edge[1]-1][a-1]
			total = l_weight + q_weight*((self.instance.n-1) / (self.instance.n*(self.instance.n-1) - 1))
			edges[edge] = (total, l_weight, 0, q_weight)
		return edges


	def h2_method(self):
		available_edges = self.initialize_contributions_h2()
		components = [i for i in range(1,self.instance.n+1)]
		i = 0
		#print(available_edges)
		while i < self.instance.n-1:
			#print()
			edge = min(available_edges.keys(), key=(lambda k: available_edges[k][0]))
			#print('escolhido ', edge)
			available_edges.pop(edge)
			self.update_components(edge, components)
			#print(components)
			self.tree.tree[edge[0]-1].append(edge[1])
			self.tree.tree[edge[1]-1].append(edge[0])
			removed = self.remove_cycles(available_edges, edge, components)
			#print(i, len(removed))
			#print('removed ', removed)
			self.update_weights_h2(i+1, available_edges, edge, removed)
			#print(available_edges)
			i += 1

		weight = 0
		for i in range(1, self.instance.n + 1):
			for j in self.tree.tree[i-1]:
				if j > i:
					weight += self.instance.l_values[i-1][j-1]
				for k in self.tree.tree[j-1]:
					if k != i:
						weight += self.instance.q_values[i-1][j-1][k-1]
		self.tree.weight = weight
		return self.tree


	def get_initial_edge(self):
		best_edge = (-1, -1)
		best_weight = sys.maxsize
		vertices = [i for i in range(1,self.instance.n+1)]
		for edge in self.instance.edges:
			l_weight = self.instance.l_values[edge[0]-1][edge[1]-1]
			q_weight = 0
			for a in vertices:
				if a not in edge:
					q_weight += 2*self.instance.q_values[edge[1]-1][edge[0]-1][a-1]
					q_weight += 2*self.instance.q_values[edge[0]-1][edge[1]-1][a-1]
			total = l_weight + q_weight
			if total < best_weight:
				best_weight = total
				best_edge = edge
		#print('initial', best_edge, best_weight)
		return best_edge


	def get_best_corner_edge(self, corner, unused_vertices):
		edge = None
		weight = sys.maxsize
		for a in unused_vertices:
			for c in corner:
				w = self.instance.l_values[c[0]-1][a-1] + 2*self.instance.q_values[a-1][c[0]-1][c[1]-1]
				if w < weight:
					weight = w
					edge = (c[0], a, w)
		return edge


	def path_method(self):
		unused_vertices = [i for i in range(1, self.instance.n + 1)]
		initial_edge = self.get_initial_edge()
		unused_vertices.remove(initial_edge[0])
		unused_vertices.remove(initial_edge[1])
		corner = [(initial_edge[0], initial_edge[1]), (initial_edge[1], initial_edge[0])]
		self.tree.tree[initial_edge[0]-1].append(initial_edge[1])
		self.tree.tree[initial_edge[1]-1].append(initial_edge[0])
		self.tree.weight = self.instance.l_values[initial_edge[0]-1][initial_edge[1]-1]
		i = 1
		while i < self.instance.n-1:
			#print('corner', corner)
			connection = self.get_best_corner_edge(corner, unused_vertices)
			#print('connection', i, connection)
			unused_vertices.remove(connection[1])
			self.tree.tree[connection[0]-1].append(connection[1])
			self.tree.tree[connection[1]-1].append(connection[0])
			self.tree.weight += connection[2]
			for c in range(len(corner)):
				if corner[c][0] == connection[0]:
					corner[c] = (connection[1], corner[c][0])
					break
			i += 1
		return self.tree


	def get_min_available_edge(self, edges, unused_vertices, used_vertices):
		best_v = -1
		chosen_v = -1
		best_w = sys.maxsize
		best_q = -1
		best_l = -1
		for v_in in used_vertices:
			for v_out in unused_vertices:
				if v_in < v_out:
					edge = (v_in, v_out)
				else:
					edge = (v_out, v_in)
				w = edges[edge][0]
				if w < best_w:
					best_w = w
					best_v = v_out
					chosen_v = v_in
					best_l = edges[edge][1]
					best_q = edges[edge][2]
		#print('best w', best_l+best_q, best_w)
		return (best_v, chosen_v, best_l + best_q)


	def update_weights_tree(self, unused_vertices, used_vertices, edge, edges, available_edges, h2):
		# (total, linear, tree_contribution, out_contribution)
		if not h2:
			for v in unused_vertices:
				increment = self.instance.q_values[v-1][edge[0]-1][edge[1]-1]*2
				if v < edge[0]:
					contribution = edges[(v, edge[0])]
					edges[(v, edge[0])] = (contribution[0] + increment, contribution[1], contribution[2] + increment, contribution[3])
				else:
					contribution = edges[(edge[0], v)]
					edges[(edge[0], v)] = (contribution[0] + increment, contribution[1], contribution[2] + increment, contribution[3])

				increment = self.instance.q_values[v-1][edge[1]-1][edge[0]-1]*2
				if v < edge[1]:
					contribution = edges[(v, edge[1])]
					edges[(v, edge[1])] = (contribution[0] + increment, contribution[1], contribution[2] + increment, contribution[3])
				else:
					contribution = edges[(edge[1], v)]
					edges[(edge[1], v)] = (contribution[0] + increment, contribution[1], contribution[2] + increment, contribution[3])
		else:
			for v in unused_vertices:
				increment = self.instance.q_values[v-1][edge[0]-1][edge[1]-1]*2
				if v < edge[0]:
					contribution = edges[(v, edge[0])]
					edges[(v, edge[0])] = (contribution[0], contribution[1], contribution[2] + increment, contribution[3] - increment)
				else:
					contribution = edges[(edge[0], v)]
					edges[(edge[0], v)] = (contribution[0], contribution[1], contribution[2] + increment, contribution[3] - increment)

				increment = self.instance.q_values[v-1][edge[1]-1][edge[0]-1]*2
				if v < edge[1]:
					contribution = edges[(v, edge[1])]
					edges[(v, edge[1])] = (contribution[0] + increment, contribution[1], contribution[2] + increment, contribution[3] - increment)
				else:
					contribution = edges[(edge[1], v)]
					edges[(edge[1], v)] = (contribution[0]  + increment, contribution[1], contribution[2] + increment, contribution[3] - increment)
				
				for v_in in used_vertices:
					if v_in not in edge:
						decrement = self.instance.q_values[v-1][edge[0]-1][v_in-1]*2
						if v < edge[0]:
							contribution = edges[(v, edge[0])]
							edges[(v, edge[0])] = (contribution[0], contribution[1], contribution[2], contribution[3] - decrement)
						else:
							contribution = edges[(edge[0], v)]
							edges[(edge[0], v)] = (contribution[0], contribution[1], contribution[2], contribution[3] - decrement)

						decrement = self.instance.q_values[v-1][v_in-1][edge[0]-1]*2
						if v < v_in:
							contribution = edges[(v, v_in)]
							edges[(v, v_in)] = (contribution[0], contribution[1], contribution[2], contribution[3] - decrement)
						else:
							contribution = edges[(v_in, v)]
							edges[(v_in, v)] = (contribution[0], contribution[1], contribution[2], contribution[3] - decrement)
				
			for v_out in unused_vertices:
				for v_in in used_vertices:
					if v_out < v_in:
						contribution = edges[(v_out, v_in)]
						new_total = contribution[1] + contribution[2] + (((self.instance.n-1-(len(used_vertices)-1))/(available_edges-1))*contribution[3])
						edges[(v_out, v_in)] = (new_total, contribution[1], contribution[2], contribution[3])
					else:
						contribution = edges[(v_in, v_out)]
						new_total = contribution[1] + contribution[2] + (((self.instance.n-1-(len(used_vertices)-1))/(available_edges-1))*contribution[3])
						edges[(v_in, v_out)] = (new_total, contribution[1], contribution[2], contribution[3])


	def initialize_contributions_tree(self):
		edges = {}
		vertices = [i for i in range(1,self.instance.n+1)]
		for edge in self.instance.edges:
			l_weight = self.instance.l_values[edge[0]-1][edge[1]-1]
			q_weight = 0
			for a in vertices:
				if a not in edge:
					q_weight += 2*self.instance.q_values[edge[1]-1][edge[0]-1][a-1]
					q_weight += 2*self.instance.q_values[edge[0]-1][edge[1]-1][a-1]
			total = l_weight + q_weight
			edges[edge] = (total, l_weight, 0, q_weight)
		return edges


	def tree_method(self, h2):
		edges = self.initialize_contributions_tree()
		unused_vertices = [i for i in range(1, self.instance.n + 1)]
		used_vertices = []
		initial_edge = min(edges.keys(), key=(lambda k: edges[k][0]))
		self.tree.tree[initial_edge[0]-1].append(initial_edge[1])
		self.tree.tree[initial_edge[1]-1].append(initial_edge[0])
		unused_vertices.remove(initial_edge[0])
		unused_vertices.remove(initial_edge[1])
		used_vertices.append(initial_edge[0])
		used_vertices.append(initial_edge[1])
		self.tree.weight = edges[initial_edge][1]
		for key in edges:
			e = edges[key]
			edges[key] = (e[1], e[1], 0, e[3])
		available_edges = len(edges)-1
		self.update_weights_tree(unused_vertices, used_vertices, initial_edge, edges, available_edges, h2)
		i = 1
		while i < self.instance.n-1:
			edge = self.get_min_available_edge(edges, unused_vertices, used_vertices)
			self.tree.tree[edge[0]-1].append(edge[1])
			self.tree.tree[edge[1]-1].append(edge[0])
			unused_vertices.remove(edge[0])
			used_vertices.append(edge[0])
			available_edges = available_edges - (len(used_vertices)-1)
			self.update_weights_tree(unused_vertices, used_vertices, edge, edges, available_edges, h2)
			self.tree.weight += edge[2]
			i += 1
		"""
		weight = 0
		for i in range(1, self.instance.n + 1):
			for j in self.tree.tree[i-1]:
				if j > i:
					weight += self.instance.l_values[i-1][j-1]
				for k in self.tree.tree[j-1]:
					if k != i:
						weight += self.instance.q_values[i-1][j-1][k-1]
		print('compare',weight, self.tree.weight)
		"""
		return self.tree


	def hybrid_method(self, threshold):
		edges = self.initialize_contributions_tree()
		unused_vertices = [i for i in range(1, self.instance.n + 1)]
		used_vertices = []
		inner_vertices = []
		leaf_vertices = []
		initial_edge = min(edges.keys(), key=(lambda k: edges[k][0]))
		self.tree.tree[initial_edge[0]-1].append(initial_edge[1])
		self.tree.tree[initial_edge[1]-1].append(initial_edge[0])
		unused_vertices.remove(initial_edge[0])
		unused_vertices.remove(initial_edge[1])
		leaf_vertices.append(initial_edge[0])
		leaf_vertices.append(initial_edge[1])
		used_vertices.append(initial_edge[0])
		used_vertices.append(initial_edge[1])
		self.tree.weight = edges[initial_edge][1]
		for key in edges:
			e = edges[key]
			edges[key] = (e[1], e[1], 0, e[3])
		self.update_weights_tree(unused_vertices, used_vertices, initial_edge, edges, 0, False)
		i = 1
		while i < self.instance.n-1:
			leaf_choice = self.get_min_available_edge(edges, unused_vertices, leaf_vertices)
			if i > 1:
				inner_choice = self.get_min_available_edge(edges, unused_vertices, inner_vertices)
			else:
				inner_choice = (-1,-1,sys.maxsize)

			if inner_choice[2] < leaf_choice[2]*threshold:
				self.tree.tree[inner_choice[0]-1].append(inner_choice[1])
				self.tree.tree[inner_choice[1]-1].append(inner_choice[0])
				unused_vertices.remove(inner_choice[0])
				used_vertices.append(inner_choice[0])
				leaf_vertices.append(inner_choice[0])
				self.update_weights_tree(unused_vertices, used_vertices, inner_choice, edges, 0, False)
				self.tree.weight += inner_choice[2]
			else:
				self.tree.tree[leaf_choice[0]-1].append(leaf_choice[1])
				self.tree.tree[leaf_choice[1]-1].append(leaf_choice[0])
				unused_vertices.remove(leaf_choice[0])
				used_vertices.append(leaf_choice[0])
				leaf_vertices.append(leaf_choice[0])
				leaf_vertices.remove(leaf_choice[1])
				inner_vertices.append(leaf_choice[1])
				self.update_weights_tree(unused_vertices, used_vertices, leaf_choice, edges, 0, False)
				self.tree.weight += leaf_choice[2]
			i += 1
		"""
		weight = 0
		for i in range(1, self.instance.n + 1):
			for j in self.tree.tree[i-1]:
				if j > i:
					weight += self.instance.l_values[i-1][j-1]
				for k in self.tree.tree[j-1]:
					if k != i:
						weight += self.instance.q_values[i-1][j-1][k-1]
		print('compare',weight, self.tree.weight)
		"""
		return self.tree