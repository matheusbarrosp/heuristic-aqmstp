import sys
import Instance
import instance_reader
import Constructive_Heuristic
import Tree
import Local_Search
import time

def check_cycle(tree):
	stack = [(1, -1)]
	visited = [False for i in range(len(tree))]
	while stack:
		v = stack.pop()
		if visited[v[0]-1] == True:
			return True
		else:
			visited[v[0]-1] = True
			for a in tree[v[0]-1]:
				if a != v[1]:
					stack.append((a,v[0]))
	cont = 0
	for i in visited:
		if i:
			cont += 1
	return cont != len(tree)

if __name__ == '__main__':
	start_time = time.time()
	instance_file = sys.argv[1]
	instance = Instance.Instance(instance_reader.read_instance(instance_file))
	constructive_heuristic = Constructive_Heuristic.Constructive_Heuristic(instance, False)
	
	#tree = constructive_heuristic.h2_method()
	#tree = constructive_heuristic.path_method()
	#tree = constructive_heuristic.tree_method(False)
	#tree = constructive_heuristic.tree_method(True)
	tree = constructive_heuristic.hybrid_method(0.8)
	print(tree)
	
	local_search = Local_Search.Local_Search(tree, instance)
	tree = local_search.local_search()
	full_time = time.time() - start_time
	print(tree)
	print(full_time)
	"""
	print(check_cycle(tree.tree))
	weight = 0
	for i in range(1, instance.n + 1):
		for j in tree.tree[i-1]:
			if j > i:
				weight += instance.l_values[i-1][j-1]
			for k in tree.tree[j-1]:
				if k != i:
					weight += instance.q_values[i-1][j-1][k-1]
	print('compare',weight, tree.weight)
	"""

