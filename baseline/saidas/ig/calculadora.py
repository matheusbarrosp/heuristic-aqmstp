import sys

f = open(sys.argv[1], 'r')

solucao = 0
tempo = 0.0
s = True
for line in f:
	if s:
		solucao += int(line)
	else:
		tempo += float(line)
	s = not s

print("& {0} & {1} \\\\".format(solucao/10,tempo/10))
