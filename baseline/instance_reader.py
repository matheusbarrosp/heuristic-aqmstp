def read_linear_values(l, l_values, edges):
	size = len(l)
	for i in range(1,size,2):
		key = l[i-1][1:-1].split(",")
		value = int(l[i])
		edges.append((int(key[0]), int(key[1])))
		key0 = int(key[0])-1
		key1 = int(key[1])-1
		l_values[key0][key1] = value
		l_values[key1][key0] = value
		

def read_quadratic_values(l, q_values):
	size = len(l)
	for i in range(1,size,2):
		key = l[i-1][1:-1].split(",")
		(a, b, c, d) = (int(key[0])-1, int(key[1])-1, int(key[2])-1, int(key[3])-1)
		if a == c:
			q_values[b][a][d] = int(l[i])
		elif a == d:
			q_values[b][a][c] = int(l[i])
		elif b == c:
			q_values[a][b][d] = int(l[i])
		else:
			q_values[a][b][c] = int(l[i])


def read_instance(file_name):
	file = open(file_name,"r")

	i = 0
	n = 0
	m = 0

	linear_values = []
	quadratic_values = []
	edges = []

	for line in file:
		if i == 0:
			n = int(line.split(" ")[-2])
			linear_values = [[0 for y in range(n)] for x in range(n)]
			quadratic_values = [[[0 for z in range(n)] for y in range(n)] for x in range(n)]
		elif i == 1:
			m = int(line.split(" ")[-2])
		elif i == 3:
			l = line.split(" ")[3:-1]
			read_linear_values(l, linear_values, edges)
		elif i == 4:
			l = line.split(" ")[3:-1]
			read_quadratic_values(l, quadratic_values)
		i += 1

	file.close()
	return (linear_values, quadratic_values, edges, n)