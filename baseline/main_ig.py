import sys
import Instance
import instance_reader
import Constructive_Heuristic
import Tree
import IG
import time

if __name__ == '__main__':
	start_time = time.time()
	instance_file = sys.argv[1]
	instance = Instance.Instance(instance_reader.read_instance(instance_file))
	constructive_heuristic = Constructive_Heuristic.Constructive_Heuristic(instance, True)
	tree = constructive_heuristic.destructive_method()
	print(tree)

	local_search = IG.IG(tree, instance)
	tree = local_search.ig()
	full_time = time.time() - start_time
	print(tree)
	print(full_time)

