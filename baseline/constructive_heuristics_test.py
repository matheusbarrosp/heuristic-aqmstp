import sys
import Instance
import instance_reader
import Constructive_Heuristic
import Tree
import Local_Search
import time

def check_cycle(tree):
	stack = [(1, -1)]
	visited = [False for i in range(len(tree))]
	while stack:
		v = stack.pop()
		if visited[v[0]-1] == True:
			return True
		else:
			visited[v[0]-1] = True
			for a in tree[v[0]-1]:
				if a != v[1]:
					stack.append((a,v[0]))
	cont = 0
	for i in visited:
		if i:
			cont += 1
	return cont != len(tree)
		


if __name__ == '__main__':
	instance_file = sys.argv[1]
	
	#IG
	print('IG')
	start_time = time.time()
	instance = Instance.Instance(instance_reader.read_instance(instance_file))
	constructive_heuristic = Constructive_Heuristic.Constructive_Heuristic(instance, True)
	tree = constructive_heuristic.destructive_method()
	local_search = Local_Search.Local_Search(tree, instance)
	tree = local_search.local_search()
	full_time = time.time() - start_time
	print(tree)
	print(full_time)
	if check_cycle(tree.tree):
		stop = input("CICLO")

	#H2
	print('\nH2')
	start_time = time.time()
	instance = Instance.Instance(instance_reader.read_instance(instance_file))
	constructive_heuristic = Constructive_Heuristic.Constructive_Heuristic(instance, False)
	tree = constructive_heuristic.h2_method()
	local_search = Local_Search.Local_Search(tree, instance)
	tree = local_search.local_search()
	full_time = time.time() - start_time
	print(tree)
	print(full_time)
	if check_cycle(tree.tree):
		stop = input("CICLO")

	#Heuristica Path
	print('\nPath')
	start_time = time.time()
	instance = Instance.Instance(instance_reader.read_instance(instance_file))
	constructive_heuristic = Constructive_Heuristic.Constructive_Heuristic(instance, False)
	tree = constructive_heuristic.path_method()
	local_search = Local_Search.Local_Search(tree, instance)
	tree = local_search.local_search()
	full_time = time.time() - start_time
	print(tree)
	print(full_time)
	if check_cycle(tree.tree):
		stop = input("CICLO")

	#Heuristica Tree C1
	print('\nTree C1')
	start_time = time.time()
	instance = Instance.Instance(instance_reader.read_instance(instance_file))
	constructive_heuristic = Constructive_Heuristic.Constructive_Heuristic(instance, False)
	tree = constructive_heuristic.tree_method(False)
	local_search = Local_Search.Local_Search(tree, instance)
	tree = local_search.local_search()
	full_time = time.time() - start_time
	print(tree)
	print(full_time)
	if check_cycle(tree.tree):
		stop = input("CICLO")

	#Heuristica Tree C2
	print('\nTree C2')
	start_time = time.time()
	instance = Instance.Instance(instance_reader.read_instance(instance_file))
	constructive_heuristic = Constructive_Heuristic.Constructive_Heuristic(instance, False)
	tree = constructive_heuristic.tree_method(True)
	local_search = Local_Search.Local_Search(tree, instance)
	tree = local_search.local_search()
	full_time = time.time() - start_time
	print(tree)
	print(full_time)
	if check_cycle(tree.tree):
		stop = input("CICLO")


	#Heuristica Tree C2
	print('\nHybrid')
	start_time = time.time()
	instance = Instance.Instance(instance_reader.read_instance(instance_file))
	constructive_heuristic = Constructive_Heuristic.Constructive_Heuristic(instance, False)
	tree = constructive_heuristic.hybrid_method(0.8)
	local_search = Local_Search.Local_Search(tree, instance)
	tree = local_search.local_search()
	full_time = time.time() - start_time
	print(tree)
	print(full_time)
	if check_cycle(tree.tree):
		stop = input("CICLO")
