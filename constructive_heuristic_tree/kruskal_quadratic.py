import instance_reader
import sys
from collections import OrderedDict

def create_sorted_graph(graph):
	return OrderedDict(sorted(graph.items(), key=lambda x: x[1]))


def kruskal(graph,n):
	tree = []
	distance = 0

	sets = [[counter+1] for counter in range(n)]
	#print sets
	i = 0
	for edge in graph:
		if(len(tree) == n-1):
			break
		else:
			#print "Edge {0}".format(edge)
			#print "sets[{0}] = {1}".format(edge[0], sets[edge[0]-1])
			#print "sets[{0}] = {1}".format(edge[1], sets[edge[1]-1])
			if sets[edge[0]-1] != sets[edge[1]-1]:
				tree.append(edge)
				#print tree
				distance += graph[edge]
				sets[edge[0]-1].extend(sets[edge[1]-1])
				for vertex in sets[edge[0]-1]: 
					sets[vertex-1] = sets[edge[0]-1]
				#print sets
		#print "\n"
	print "Kruskal: "
	print tree
	print distance
	return tree, distance


def calculate_quadratic_distance(tree, q_values, linear_value):
	q_distance = 0
	size = len(tree)
	for current_edge in tree:
		for edge in tree:
			if current_edge != edge:
				key = (current_edge[0], current_edge[1], edge[0], edge[1])
				if q_values.has_key(key):
					q_distance += q_values[key]
					#print "Edges {0} - {1}: {2}".format(current_edge, tree[index], q_values["[{0},{1},{2},{3}]".format(current_edge[0], current_edge[1], tree[index][0], tree[index][1])])
				#else:
					#print "key {0} not found".format(key)
	return q_distance + linear_value


if __name__ == '__main__':

	file = sys.argv[1]

	instance = instance_reader.read_instance(file)
	l_values = instance[0]
	q_values = instance[1]
	n = instance[2]

	tree = kruskal(create_sorted_graph(l_values),n)
	print "\nQuadratic distance:"
	print calculate_quadratic_distance(tree[0], q_values, tree[1])