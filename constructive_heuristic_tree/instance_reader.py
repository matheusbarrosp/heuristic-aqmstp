def read_linear_values(list):
	l_values = {}
	size = len(list)
	for i in range(1,size,2):
		key = list[i-1][1:-1].split(",")
		l_values[(int(key[0]), int(key[1]))] = int(list[i])

	return l_values


def read_quadratic_values(list):
	q_values = {}
	size = len(list)
	for i in range(1,size,2):
		key = list[i-1][1:-1].split(",")
		q_values[(int(key[0]), int(key[1]), int(key[2]), int(key[3]))] = int(list[i])

	return q_values


def read_instance(file_name):
	file = open(file_name,"r")

	i = 0
	n = 0
	m = 0

	linear_values = {}
	quadratic_values = {}

	for line in file:
		if i == 0:
			n = int(line.split(" ")[-2])
		elif i == 1:
			m = int(line.split(" ")[-2])
		elif i == 3:
			l = line.split(" ")[3:-1]
			linear_values = read_linear_values(l)
		elif i == 4:
			l = line.split(" ")[3:-1]
			quadratic_values = read_quadratic_values(l)
		i += 1

	file.close()
	return linear_values, quadratic_values, n, m