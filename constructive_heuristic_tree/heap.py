import instance_reader
import sys
from collections import OrderedDict
import heapq

def create_sorted_dict(dictionary):
	return OrderedDict(sorted(dictionary.items(), key=lambda x: x[1]))


def create_heap(edge, n, l_values, q_values):
	heap = []
	for x in range (1,n+1):
		if x not in edge:
			if (edge[0],x) in l_values:
				c = l_values[(edge[0], x)] + q_values[(edge[0], x, edge[0], edge[1])]
				heap.append([c,x,edge[0]])
			else:
				c = l_values[(x,edge[0])] + q_values[(x, edge[0], edge[0], edge[1])]
				heap.append([c,x,edge[0]])
	
	for x in heap:
		if (edge[1],x[1]) in l_values:
			c = l_values[(edge[1], x[1])] + q_values[(edge[1], x[1], edge[0], edge[1])]
		else:
			c = l_values[(x[1],edge[1])] + q_values[(x[1], edge[1], edge[0], edge[1])]
		if c < x[0]:
			x[0] = c
			x[2] = edge[1]

	heapq.heapify(heap)
	return heap


def adjacent_values(edge, tree, q_values):
	#print("Adjacent values of {0}".format(edge))
	sum = 0
	for e in tree:
		if (edge[0], edge[1], e[0], e[1]) in q_values:
			#print("adjacent to {0}: {1}".format((e[0],e[1]), q_values[(edge[0], edge[1], e[0], e[1])]))
			sum += q_values[(edge[0], edge[1], e[0], e[1])]
	#print("Sum: {0}".format(sum))
	return sum


def used_vertex(tree):
	s = set()
	for edge in tree:
		for vertex in edge:
			s.add(vertex)
	return s


def update_heap(heap, tree, l_values, q_values):
	#print("Updating heap")
	v = used_vertex(tree)

	for x in heap:
		for vertex in v:
			if x[2] == vertex:
				#print("Atualizando {0}".format(x))
				if (vertex,x[1]) in l_values:
					c = l_values[(vertex, x[1])] + adjacent_values((vertex, x[1]), tree, q_values)
				else:
					c = l_values[(x[1],vertex)] + adjacent_values((x[1], vertex), tree, q_values)
				#print("Novo c: {0}".format(c))
				x[0] = c

	for x in heap:
		for vertex in v:
			#print("x: {0}, Edge: {1}, Vertex: {2}".format(x,x[1],vertex))
			if (vertex,x[1]) in l_values:
				c = l_values[(vertex, x[1])] + adjacent_values((vertex, x[1]), tree, q_values)
			else:
				c = l_values[(x[1],vertex)] + adjacent_values((x[1], vertex), tree, q_values)
			#print("c = {0}".format(c))
			if c < x[0]:
				#print("{0} entrou na condicao".format(vertex))
				x[0] = c
				x[2] = vertex
	heapq.heapify(heap)


def create_tree(l_values, q_values, n):
	initial_edge = list(l_values.keys())[0]
	heap = create_heap(initial_edge, n, l_values, q_values)
	tree = []
	tree.append(initial_edge)

	#print("Tree")
	#print(tree)
	#print("Heap")
	#print(heap)
	#print("\n")
	
	while len(heap) != 0:
		e = heapq.heappop(heap)
		if (e[1], e[2]) in l_values:
			tree.append((e[1],e[2]))
		else:
			tree.append((e[2], e[1]))
		#print("Tree")
		#print(tree)
		update_heap(heap, tree, l_values, q_values)
		#print("Heap")
		#print(heap)
		#print("\n")
	
	return tree


def calculate_weight(tree, l_values, q_values):
	weight = 0
	for edge1 in tree:
		weight += l_values[edge1]
		for edge2 in tree:
			if edge1 != edge2:
				if (edge1[0], edge1[1], edge2[0], edge2[1]) in q_values:
					weight += q_values[(edge1[0], edge1[1], edge2[0], edge2[1])]

	return weight


if __name__ == '__main__':

	file = sys.argv[1]

	instance = instance_reader.read_instance(file)
	l_values = instance[0]
	q_values = instance[1]
	n = instance[2]

	#create_graph(l_values, q_values)
	l_values = create_sorted_dict(l_values)

	tree = create_tree(l_values, q_values, n)
	print(tree)
	print(calculate_weight(tree, l_values, q_values))