import instance_reader
import sys
from collections import OrderedDict

def create_sorted_dict(dictionary):
	return OrderedDict(sorted(dictionary.items(), key=lambda x: x[1]))


def update_contribution(edge, unused_edges, q_values):
	for e in unused_edges:
		if edge[0] == e[0] or edge[0] == e[1] or edge[1] == e[0] or edge[1] == e[1]:
			if (edge[0], edge[1], e[0], e[1]) in q_values:
				unused_edges[e] += q_values[(edge[0], edge[1], e[0], e[1])]
			elif (e[0], e[1], edge[0], edge[1]) in q_values:
				unused_edges[e] += q_values[(e[0], e[1], edge[0], edge[1])]	


def create_tree(l_values, q_values, n):
	#Initialize variables
	unused_edges = dict(l_values.copy())
	tree = []
	sets = [i for i in range(1,n+1)]

	#Choose minimal edge as initial edge
	#print(unused_edges)
	initial_edge = list(l_values.keys())[0]
	unused_edges.pop(initial_edge)
	#print(unused_edges)
	sets[initial_edge[0]-1] = sets[initial_edge[1]-1]
	tree.append(initial_edge)
	update_contribution(initial_edge, unused_edges, q_values)
	#print(unused_edges)
	i = 1
	#print(tree)
	#print("Comeca loop")
	while i < n - 1:
		#Order dictionary by values
		ordered = create_sorted_dict(unused_edges)
		#print()
		#print(ordered)
		#Find the minimal adjacent edge that does not creates a cycle
		cycle = True
		index = 0
		minimal_edge = list(ordered.keys())[0]
		while cycle:
			minimal_edge = list(ordered.keys())[index]
			#print("Minimal edge: {0}".format(minimal_edge))
			if is_adjacent(minimal_edge, tree):
				#print("It is adjacent")
				unused_edges.pop(minimal_edge)
				ordered.pop(minimal_edge)
				cycle = sets[minimal_edge[0]-1] == sets[minimal_edge[1]-1]
				#if cycle:
				#	print("Cycle")
				#else:
				#	print("No Cycle")
			else:
				#print("Not adjacent")
				index += 1

		#Add minimal edge in tree list
		unite_sets(minimal_edge, sets)
		tree.append(minimal_edge)
		#print("Tree: {0}".format(tree))
		#Update all unused edges contribution based on the new edge added
		update_contribution(minimal_edge, unused_edges, q_values)
		i += 1
	return tree


def is_adjacent(edge, tree):
	for e in tree:
		if edge[0] == e[0] or edge[0] == e[1] or edge[1] == e[0] or edge[1] == e[1]:
			return True
	return False 


def unite_sets(edge, sets):
	to_change = sets[edge[0]-1]
	vertex = sets[edge[1]-1]
	for i in range(len(sets)):
		if sets[i] == to_change:
			sets[i] = vertex

	
def create_graph(l_values, q_values):
	for item in l_values.items():
		key = item[0]
		value = item[1]
		q_values[(key[0], key[1], key[0], key[1])] = value


def calculate_weight(tree, l_values, q_values):
	weight = 0
	for edge1 in tree:
		weight += l_values[edge1]
		for edge2 in tree:
			if edge1 != edge2:
				if (edge1[0], edge1[1], edge2[0], edge2[1]) in q_values:
					weight += q_values[(edge1[0], edge1[1], edge2[0], edge2[1])]

	return weight


if __name__ == '__main__':

	file = sys.argv[1]

	instance = instance_reader.read_instance(file)
	l_values = instance[0]
	q_values = instance[1]
	n = instance[2]

	#create_graph(l_values, q_values)
	l_values = create_sorted_dict(l_values)
	q_values = create_sorted_dict(q_values)

	tree = create_tree(l_values, q_values, n)
	print(tree)
	print(calculate_weight(tree, l_values, q_values))