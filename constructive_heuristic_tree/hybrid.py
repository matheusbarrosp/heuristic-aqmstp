import instance_reader
import sys
from collections import OrderedDict

class Instance:
	def __init__(self, instance):
		self.l_values = self.__create_ordered_dict(instance[0])
		self.q_values = instance[1]
		self.n = instance[2]


	def __create_ordered_dict(self, dictionary):
		return OrderedDict(sorted(dictionary.items(), key=lambda x: x[1]))


class Tree:
	def __init__(self, instance, threshold):
		self.instance = instance
		self.tree = []
		self.threshold = threshold


	def adjacent_vertexes(self, vertex):	
		vertexes = set()
		for edge in self.tree:
			if vertex in edge:
				if edge[0] != vertex:
					vertexes.add(edge[0])
				else:
					vertexes.add(edge[1])
		return vertexes


	def calculate_weight(self, vertex, vertex_in_tree):
		vertexes = self.adjacent_vertexes(vertex_in_tree)
		a = vertex
		b = vertex_in_tree
		if vertex > vertex_in_tree:
			a = vertex_in_tree
			b = vertex
		weight = self.instance.l_values[(a,b)]
		for adjacent in vertexes:
			x = adjacent
			y = vertex_in_tree
			if adjacent > vertex_in_tree:
				x = vertex_in_tree
				y = adjacent
			if (a,b,x,y) in self.instance.q_values:
				weight += self.instance.q_values[(a,b,x,y)]
		return weight


	def find_best_vertex(self, corner, unused_vertexes):
		best_vertex = unused_vertexes[0]
		best_weight = self.calculate_weight(best_vertex, corner)
		for vertex in unused_vertexes[1:]:
			weight = self.calculate_weight(vertex, corner)
			if weight < best_weight:
				best_weight = weight
				best_vertex = vertex
		return (best_vertex, best_weight)


	def vertexes_in_tree(self):
		vertexes = set()
		for edge in self.tree:
			vertexes.add(edge[0])
			vertexes.add(edge[1])
		return vertexes


	def find_best_inner_vertex(self, unused_vertexes, corners):
		#print()
		#print("Finding best vertex")
		best_weight = -1
		best_vertex = -1
		chosen_inner = -1
		in_tree = self.vertexes_in_tree()
		#print(in_tree)
		for inner_vertex in in_tree:
			if inner_vertex not in corners:
				current_weight = 0
				for outer_vertex in unused_vertexes:
					current_weight = self.calculate_weight(outer_vertex, inner_vertex)
					#print(outer_vertex, inner_vertex, current_weight)
					if current_weight < best_weight or best_weight == -1:
						best_weight = current_weight
						best_vertex = outer_vertex
						chosen_inner = inner_vertex
		return best_vertex, chosen_inner, best_weight


	def create_tree(self):
		initial_edge = list(self.instance.l_values.keys())[0]
		unused_vertexes = [i for i in range(1,self.instance.n + 1)]
		unused_vertexes.remove(initial_edge[0])
		unused_vertexes.remove(initial_edge[1])

		self.tree.append(initial_edge)
		corners = [initial_edge[0], initial_edge[1]]
		i = len(unused_vertexes)
		first_iteration = True
		while i > 0:
			option1 = self.find_best_vertex(corners[0], unused_vertexes)
			option2 = self.find_best_vertex(corners[1], unused_vertexes)
			choice = (option1[0], corners[0], option1[1])
			if option1[1] > option2[1]:
				choice = (option2[0], corners[1], option2[1])
			if not first_iteration:
				inner_option = self.find_best_inner_vertex(unused_vertexes, corners)
				#print(inner_option)
				#print(choice)

				if inner_option[2] < threshold*choice[2]:
					choice = inner_option
			else:
				first_iteration = False

			if (choice[0], choice[1]) in self.instance.l_values:
				self.tree.append((choice[0], choice[1]))
			else:
				self.tree.append((choice[1], choice[0]))

			if choice[1] == corners[0]:
				corners[0] = choice[0]
			elif choice[1] == corners[1]:
				corners[1] = choice[0]
			#print(self.tree)
			#print(unused_vertexes)
			#print(corners)
			unused_vertexes.remove(choice[0])
			i -= 1


	def calculate_total_weight(self):
		weight = 0
		for edge1 in self.tree:
			weight += self.instance.l_values[edge1]
			for edge2 in self.tree:
				if edge1 != edge2:
					if (edge1[0], edge1[1], edge2[0], edge2[1]) in self.instance.q_values:
						weight += self.instance.q_values[(edge1[0], edge1[1], edge2[0], edge2[1])]

		return weight


	def print(self):
		print(self.tree)
		print(self.calculate_total_weight())


if __name__ == '__main__':
	threshold = float(sys.argv[1])
	file = sys.argv[2]
	instance = instance_reader.read_instance(file)
	tree = Tree(Instance(instance), threshold)

	tree.create_tree()
	tree.print()